package com.aliengain.ug;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

/**
 * Created by admin on 09.03.2017.
 */
@RunWith(Parameterized.class)
public class SignUpFlowTest {

  private static WebDriver driver;
  private static WebDriverWait wait;
  private String serverUrl = "http://ug.aliengain.com";
  private static final int timeout = 15;
  private String acceptablePassword = "Pass123";

  private By inputPhoneFieldLocator = By.id("input_phone");
  private By inputPasswordFieldLocator = By.id("Input-Password");
  private By inputVerificationCodeLocator = By.id("Input-Code");

  private By verificationPageLocator = By.id("Verification-Form");
  private By lostPasswordPageLocator = By.id("Lost-Password-Form");

  private By errorMessageLocator = By.className ("error");

  private By joinNowButtonMainPageLocator = By.xpath("//a[@class='button yellow full']");
  private By joinNowButtonSignUpPagelocator =
      By.xpath("//form[@id='signup_step_0']/div[@class='element']/input[@name='btn_a']");
  private By verifyAccountButtonLocator =
      By.xpath("//div[@class='element']/input[@value='Verify Account']");
  private By requestNewPinButtonLocator =
      By.xpath("//div[@class='element']/input[@value='Request New Pin']");
  private By requestNewPasswordButtonLocator =
      By.xpath("//form[@id='Lost-Password-Form']/div[@class='element']/input[@name='btn_a']");
  private By inputVerificationCodeButtonLocator = By.id("Verification-Code--Button");

  private By contactSupportLinkLocator = By.xpath("//div/a[text()='contact support']");
  private By forgotPasswordLinkLocator = By.xpath("//div/a[text()='forgot password?']");
  private By termsAndConditionsLinkLocator = By.xpath("//form[@id='signup_step_0']" +
      "/div[@class='element']/p[@class='info']/a[@class='underline']");

  @Parameterized.Parameters (name = "screen dimensions ({0}:{1})")
  public static Collection<Object[]> ranges() {
    return Arrays.asList(new Object[][]{
        {1024, 768}, {360, 640}
    });
  }

  private int screenWidth;
  private int screenHeight;

  public SignUpFlowTest(int screenWidth, int screenHeight) {
    this.screenWidth = screenWidth;
    this.screenHeight = screenHeight;
  }

  @BeforeClass
  public static void startTest() {
    ChromeDriverManager.getInstance().setup();
    driver = new ChromeDriver();
    wait = new WebDriverWait(driver, timeout);
  }

  @Before
  public void initialize(){
    driver.manage().window().setSize(new Dimension(screenWidth, screenHeight));
  }

  @Test
  public void testJoinNowButtonOnMainPage() {
    openMainPage();

    WebElement joinNowButton = driver.findElement(joinNowButtonMainPageLocator);

    assertEquals(joinNowButton.isDisplayed(),true);

    verifyElementColor("#fdcd00",joinNowButton);

    verifyButtonTextAndTextTransform("Join now",joinNowButton);

    joinNowButton.click();
    verifyPageOpening("/signup",1);
  }

  @Test
  public void testJoinNowLinkOnMainPage() {
    Assume.assumeTrue(screenWidth >= 768);

    openMainPage();

    WebElement joinNowLink = driver.findElement(
        By.xpath("//a[@class='underline'][@href='signup']"));

    verifyLink("Join Now","/signup",1,joinNowLink);
  }

  @Test
  public void testMenuJoinNowButton() {
    openMainPage();

    driver.findElement(By.id("Main-Menu-Button")).click();

    String menuJoinNowElementXpath = "//div[@class='bp-dropdown-wrapper']/ul/li/a[@href='signup']";
    WebElement menuJoinNowElement = driver.findElement(By.xpath(menuJoinNowElementXpath));

    verifyElementColor("#000000", menuJoinNowElement);

    menuJoinNowElement.click();
    verifyPageOpening("/signup",1);

    driver.findElement(By.id("Main-Menu-Button")).click();
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
        By.xpath(menuJoinNowElementXpath)));
    menuJoinNowElement = driver.findElement(By.xpath(menuJoinNowElementXpath));

    verifyElementColor("#76c036", menuJoinNowElement);
  }

  @Test
  public void testSignUpPageTextElements() {
    proceedToSignUpPage();

    WebElement signUpForm = driver.findElement(By.id("signup_step_0"));
    WebElement joinNowText = driver.findElement(By.xpath("//form[@id='signup_step_0']/h2"));
    WebElement yourPhoneNumberText = driver.findElement(
        By.xpath("//form[@id='signup_step_0']/div[@class='element']/label[@for='input_phone']"));
    WebElement infoText = driver.findElement(
        By.xpath("//form[@id='signup_step_0']/div[@class='element']/p[@class='info']"));

    assertEquals(signUpForm.isDisplayed(),true);
    assertEquals("Join Now",joinNowText.getText());
    assertEquals("Your phone number:",yourPhoneNumberText.getText());
    assertEquals("By creating an account you accept the Terms and Conditions.",infoText.getText());
  }

  @Test
  public void testInputPhoneNumberField() {
    proceedToSignUpPage();

    WebElement ugandaFlagAndCode = driver.findElement(By.className("UG-flag"));
    WebElement inputPhoneField = driver.findElement(inputPhoneFieldLocator);

    assertTrue(inputPhoneField.isDisplayed());
    assertTrue(ugandaFlagAndCode.isDisplayed());
    assertEquals("+256",ugandaFlagAndCode.getText());

    String testValue = "478124010240103554";
    inputPhoneField.sendKeys(testValue);
    assertEquals(testValue,inputPhoneField.getAttribute("value"));

    inputPhoneField.clear();
    assertEquals("",inputPhoneField.getAttribute("value"));
  }

  @Test
  public void testInputPasswordField() {
    proceedToSignUpPage();

    WebElement inputPasswordField = driver.findElement(inputPasswordFieldLocator);

    assertTrue(inputPasswordField.isDisplayed());

    String testValue = "+TestP@ssw%rDF1&#!";
    inputPasswordField.sendKeys(testValue);
    assertEquals(testValue,inputPasswordField.getAttribute("value"));

    inputPasswordField.clear();
    assertEquals("",inputPasswordField.getAttribute("value"));

    assertEquals("password",inputPasswordField.getAttribute("type"));
  }

  @Test
  public void testTermsAndConditionsLink() {
    proceedToSignUpPage();

    WebElement termsAndConditionsLink = driver.findElement(termsAndConditionsLinkLocator);

    verifyLink("Terms and Conditions","/terms",2,termsAndConditionsLink);
  }

  @Test
  public void testSignUpPageJoinNowButton() {
    proceedToSignUpPage();

    WebElement joinNowButton = driver.findElement(joinNowButtonSignUpPagelocator);

    assertEquals(joinNowButton.isDisplayed(),true);

    verifyElementColor("#76c036",joinNowButton);

    verifyButtonTextAndTextTransform("Join now",joinNowButton);
  }

  @Test
  public void testSignUpWithPermittedAreaCodes() {
    Scanner s = null;

    try {
      s = new Scanner(new File("area_codes.txt"));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    ArrayList<String> areaCodesList = new ArrayList();

    while (s.hasNext()){
      areaCodesList.add(s.next());
    }

    s.close();

    for (String areaCode : areaCodesList) {
      int maxTries = 5;

      for (int i = 0; i < maxTries ; i++){
        String phoneNumber = generatePhoneNumber(areaCode);
        String password = generatePassword();

        proceedToVerificationPage(phoneNumber,password);
        checkVerificationPage(phoneNumber);

        if (!returnsTrueIfAccountAlreadyExist(phoneNumber,password)) break;

        if (i == maxTries-1) {
          fail("Failed to sign up "+maxTries+" times in a row with different phone numbers.");
        }
      }
    }
  }

  @Test
  public void testSignUpWithUgandaCountryCodeInNumber() {

    for(int i=0; i<=2; i++){
      String phoneNumber = generatePhoneNumber("4");
      String password = generatePassword();

      proceedToVerificationPage("+256"+phoneNumber,password);
      checkVerificationPage(phoneNumber);

      if (!returnsTrueIfAccountAlreadyExist(phoneNumber,password)) break;
    }
  }

  @Test
  public void testSignUpWithForeignCountryCodeInNumber() {
    String phoneNumber = "+37258044190";
    proceedToVerificationPage(phoneNumber,acceptablePassword);

    verifyErrorMessage("We do currently only allow sign up for " +
        "users with a mobile number from Uganda.");
  }

  @Test
  public void testSignUpWithInvalidNumber() {
    String[] invalidNumbers = new String[] {"202454566","45633456","4563345667"};

    for (String phoneNumber : invalidNumbers) {
      proceedToVerificationPage(phoneNumber,acceptablePassword);

      verifyErrorMessage(phoneNumber+" is not a valid phone number.");
    }
  }

  @Test
  public void testSignUpWithAlreadyRegisteredPhoneNumber() {
    String phoneNumber;

    phoneNumber = proceedToVerificationPageRetryIfAccountAlreadyExist();
    proceedToVerificationPage(phoneNumber,acceptablePassword);
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(verificationPageLocator));
    checkVerificationPage(phoneNumber);

    String anotherPassword = "Test123";

    proceedToVerificationPage(phoneNumber,anotherPassword);

    verifyErrorMessage("Account already exist for this phone number, forgot password?");

    WebElement forgotPasswordLink = driver.findElement(forgotPasswordLinkLocator);

    verifyLink("forgot password?", "/lostpw-256"+phoneNumber, 1, forgotPasswordLink);
  }

  @Test
  public void testSignUpForgotPasswordPage() {
    String phoneNumber = generatePhoneNumber("4");

    proceedToForgotPasswordPage(phoneNumber);
    verifyPageOpening("/lostpw-256"+phoneNumber,1);

    verifyInputPhoneFieldContainsPnoneNumberFlagCode(phoneNumber);

    WebElement requestNewPasswordText = driver.findElement(lostPasswordPageLocator);
    WebElement yourPhoneNumberText = driver.findElement(
        By.xpath("//form[@id='Lost-Password-Form']/div[@class='element']/label[@for='input_phone']"));
    WebElement requestNewPasswordButton = driver.findElement(requestNewPasswordButtonLocator);
    WebElement inputVerificationCodeButton = driver.findElement(inputVerificationCodeButtonLocator);

    assertTrue(requestNewPasswordText.isDisplayed());
    assertTrue(yourPhoneNumberText.isDisplayed());
    assertTrue(requestNewPasswordButton.isDisplayed());
    assertTrue(inputVerificationCodeButton.isDisplayed());

    verifyButtonTextAndTextTransform("Request New Password",requestNewPasswordButton);
    verifyButtonTextAndTextTransform("Input Verification Code",inputVerificationCodeButton);

    verifyElementColor("#76c036",requestNewPasswordButton);
    verifyElementColor("#dadada",inputVerificationCodeButton);
  }

  @Test
  public void testSignUpRequestNewPasswordButton() {
    String phoneNumber = generatePhoneNumber("4");

    proceedToForgotPasswordPage(phoneNumber);

    driver.findElement(By.name("btn_a")).click();

    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(verificationPageLocator));
    checkVerificationPage(phoneNumber);

    for (int i=0; i <= 1; i++){
      wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(verificationPageLocator));
      driver.navigate().back();
      wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(lostPasswordPageLocator));
      verifyPageOpening("/lostpw-256"+phoneNumber,1);
      driver.findElement(requestNewPasswordButtonLocator).click();
    }

    verifyErrorMessage("You have had three requests the last three hours. " +
        "Please contact support to recover your password.");

    WebElement contactSupportLink = driver.findElement(contactSupportLinkLocator);

    verifyLink("contact support", "/help",1, contactSupportLink);
  }

  @Test
  public void testSignUpInputVerificationCodeButton() {
    String phoneNumber = generatePhoneNumber("4");

    proceedToForgotPasswordPage(phoneNumber);

    WebElement inputVerificationCodeButton = driver.findElement(inputVerificationCodeButtonLocator);
    inputVerificationCodeButton.click();

    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(verificationPageLocator));
    checkVerificationPage(phoneNumber);
  }

  @Test
  public void testSignUpWithDifferentPasswords() {
    String [] validPassword =  new String[] {"991274","passwo","!@#$3416teST*^pass"};
    String passwordLessCharThanRequired = "passw";

    String phoneNumber = generatePhoneNumber("4");
    proceedToVerificationPage(phoneNumber,passwordLessCharThanRequired);
    verifyErrorMessage("Your password must be at least 6 characters.");

    for(String password : validPassword){
      int maxTries = 5;

      for (int i = 0; i < maxTries ; i++){
        phoneNumber = generatePhoneNumber("4");
        proceedToVerificationPage(phoneNumber,password);
        checkVerificationPage(phoneNumber);

        if (!returnsTrueIfAccountAlreadyExist(phoneNumber,password)) break;

        if (i == maxTries-1) {
          fail("Failed to sign up "+maxTries+" times in a row with different phone numbers.");
        }
      }
    }
  }

  @Test
  public void testAccountVerificationPageElements() {
    proceedToVerificationPageRetryIfAccountAlreadyExist();

    WebElement yourPhoneNumberText = driver.findElement(
        By.xpath("//form[@id='Verification-Form']/div[@class='element']/label[@for='input_phone']"));
    WebElement verficationCode4DigitsText = driver.findElement(
        By.xpath("//form[@id='Verification-Form']/div[@class='element']/label[@for='Input-Code']"));
    WebElement inputCodeField = driver.findElement(inputVerificationCodeLocator);
    WebElement verifyAccountButton = driver.findElement(verifyAccountButtonLocator);

    wait.until(ExpectedConditions.invisibilityOfElementLocated(requestNewPinButtonLocator));

    assertTrue(yourPhoneNumberText.isDisplayed());
    assertTrue(verficationCode4DigitsText.isDisplayed());
    assertTrue(inputCodeField.isDisplayed());
    assertTrue(verifyAccountButton.isDisplayed());
  }

  @Test
  public void testInputCodeField() {
    proceedToVerificationPageRetryIfAccountAlreadyExist();

    WebElement inputCodeField = driver.findElement(inputVerificationCodeLocator);

    assertEquals("* * * *",inputCodeField.getAttribute("placeholder"));
    assertEquals("tel",inputCodeField.getAttribute("type"));

    String pinValue = "1234";
    inputCodeField.sendKeys(pinValue);

    assertEquals(pinValue,inputCodeField.getAttribute("value"));

    inputCodeField.clear();
    assertEquals("",inputCodeField.getAttribute("value"));
  }

  @Test
  public void testVerifyingAccountWithEmptyPinOrLettersInPin() {
    String phoneNumber = proceedToVerificationPageRetryIfAccountAlreadyExist();

    WebElement inputCodeField = driver.findElement(inputVerificationCodeLocator);

    inputCodeField.sendKeys("qwer");

    WebElement verifyAccountButton = driver.findElement(verifyAccountButtonLocator);

    String errorMessageText = "The verification code must be a number.";

    verifyAccountButton.click();
    verifyErrorMessage(errorMessageText);

    inputCodeField.clear();

    verifyAccountButton.click();
    verifyErrorMessage(errorMessageText);

    checkVerificationPage(phoneNumber);
  }

  @Test
  public void testVerifyingAccountWithInvalidPin() {
    String[] invalidPinCodes = new String[] {"0000","000","00000"};

    String phoneNumber = proceedToVerificationPageRetryIfAccountAlreadyExist();

    WebElement inputCodeField = driver.findElement(inputVerificationCodeLocator);
    WebElement verifyAccountButton = driver.findElement(verifyAccountButtonLocator);

    for (String invalidPin : invalidPinCodes) {
      inputCodeField.clear();
      inputCodeField.sendKeys(invalidPin);
      verifyAccountButton.click();
      verifyErrorMessage(
          "Wrong verification code. If you have lost your code you can order a new code below.");
      wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(requestNewPinButtonLocator));
      checkVerificationPage(phoneNumber);
    }
  }

  @Test
  public void testRequestNewPinButton() {
    String phoneNumber = proceedToVerificationPageRetryIfAccountAlreadyExist();

    WebElement inputCodeField = driver.findElement(inputVerificationCodeLocator);
    WebElement verifyAccountButton = driver.findElement(verifyAccountButtonLocator);

    inputCodeField.sendKeys("0000");
    verifyAccountButton.click();

    wait.until(ExpectedConditions.visibilityOfElementLocated(requestNewPinButtonLocator));
    WebElement requestNewPinButton = driver.findElement(requestNewPinButtonLocator);

    verifyButtonTextAndTextTransform("Request New Pin",requestNewPinButton);

    verifyElementColor("#76c036",requestNewPinButton);

    requestNewPinButton.click();
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("success")));

    WebElement successMessage = driver.findElement(By.className("success"));

    verifyElementColor("#e1f6b3",successMessage);

    assertEquals("Verification code sent", successMessage.getText());

    checkVerificationPage(phoneNumber);
  }

  @Test
  public void testPinRequestedThreeTimes() {
    proceedToVerificationPageRetryIfAccountAlreadyExist();

    WebElement inputCodeField = driver.findElement(inputVerificationCodeLocator);

    for (int i = 0; i<=2 ; i++) {
      wait.until(ExpectedConditions.visibilityOfElementLocated(verifyAccountButtonLocator));
      WebElement verifyAccountButton = driver.findElement(verifyAccountButtonLocator);
      inputCodeField.clear();
      inputCodeField.sendKeys("0000");

      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      verifyAccountButton.click();

      if (i == 2) break;

      wait.until(ExpectedConditions.visibilityOfElementLocated(requestNewPinButtonLocator));
      WebElement requestNewPinButton = driver.findElement(requestNewPinButtonLocator);
      requestNewPinButton.click();
    }

    wait.until(ExpectedConditions.visibilityOfElementLocated(errorMessageLocator));

    verifyErrorMessage("Wrong verification code. Please contact support to recover your code.");

    WebElement contactSupportLink = driver.findElement(contactSupportLinkLocator);

    verifyLink("contact support","/help",1,contactSupportLink);
  }

  @After
  public void checkWindowHandlesAfterTest() {
    closeTabs();
  }

  @AfterClass
  public static void endTest() {
    driver.quit();
  }

  public void openMainPage() {
    driver.get(serverUrl);
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("bp-after-fixed")));
  }

  public void verifyElementColor(String expectedColor, WebElement element) {
    String actualColor = Color.fromString(element.getCssValue("background-color")).asHex();

    assertEquals(expectedColor, actualColor);
  }

  public void verifyPageOpening(String parameter, int expectedNumberOfWindows) {
    if (expectedNumberOfWindows == 1) {
      wait.until(ExpectedConditions.urlToBe(serverUrl+parameter));
    } else {
      wait.until(ExpectedConditions.numberOfWindowsToBe(expectedNumberOfWindows));
    }

    if (driver.getWindowHandles().size()>1) {

      for(String winHandle : driver.getWindowHandles()){
        driver.switchTo().window(winHandle);
      }
    }

    String expectedSignUpUrl = serverUrl+parameter;
    String actualSignUpUrl = driver.getCurrentUrl();
    assertThat(actualSignUpUrl, equalTo(expectedSignUpUrl));

    if (driver.getWindowHandles().size() != expectedNumberOfWindows) {
      fail("Incorrect number of windows were opened");
    }

    closeTabs();
  }

  public void closeTabs() {
    if (driver.getWindowHandles().size()>1) {
      String windowHandleMain = driver.getWindowHandle();

      for (String handle : driver.getWindowHandles()) {

        if (!handle.equals(windowHandleMain)) {
          driver.switchTo().window(handle);
          driver.close();
        }
      }

      driver.switchTo().window(windowHandleMain);
    }
  }

  public String generatePhoneNumber(String areaCode) {
    String randomNumber =
        Integer.toString(10000000 + (int)(Math.random() * ((99999999 - 10000000 + 1))));
    String phoneNumber = areaCode+randomNumber;
    phoneNumber = phoneNumber.substring(0,9);
    return phoneNumber;
  }

  public String generatePassword() {
    String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345" +
        "6789~!@#$%^&*()-=+?";
    return RandomStringUtils.random( 10, characters );
  }

  public void proceedToSignUpPage() {
    driver.get(serverUrl);

    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(joinNowButtonMainPageLocator));

    try {
      Thread.sleep(500);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    driver.findElement(joinNowButtonMainPageLocator).click();

    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(joinNowButtonSignUpPagelocator));
  }

  public void proceedToVerificationPage(String phoneNumber, String password) {
    proceedToSignUpPage();

    driver.findElement(inputPhoneFieldLocator).sendKeys(phoneNumber);
    driver.findElement(inputPasswordFieldLocator).sendKeys(password);
    driver.findElement(joinNowButtonSignUpPagelocator).click();
    wait.until(ExpectedConditions.or(
        ExpectedConditions.visibilityOfAllElementsLocatedBy(errorMessageLocator),
        ExpectedConditions.visibilityOfAllElementsLocatedBy(verificationPageLocator)));
  }

  public void checkVerificationPage(String phoneNumber) {
    if (driver.findElements(verificationPageLocator).size() != 0) {
      verifyPageOpening("/vcode-256"+phoneNumber,1);
      verifyInputPhoneFieldContainsPnoneNumberFlagCode(phoneNumber);
    }
  }

  public void verifyErrorMessage(String expectedErrorText ){
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(errorMessageLocator));
    WebElement errorMessage = driver.findElement(errorMessageLocator);

    verifyElementColor("#ffcccc",errorMessage);

    assertEquals(expectedErrorText,errorMessage.getText());
  }

  public void verifyInputPhoneFieldContainsPnoneNumberFlagCode(String phoneNumber) {
    WebElement ugandaFlagAndCode = driver.findElement(By.className("UG-flag"));
    WebElement inputPhoneField = driver.findElement(inputPhoneFieldLocator);

    assertTrue(inputPhoneField.isDisplayed());
    assertTrue(ugandaFlagAndCode.isDisplayed());

    assertEquals("+256",ugandaFlagAndCode.getText());
    assertEquals(phoneNumber, inputPhoneField.getAttribute("value"));
  }

  public void verifyLink(
      String expectedLinkText,String expectedUrlParameter,
      int expectedNumberOfWindows, WebElement link) {

    assertEquals(link.isDisplayed(),true);
    assertEquals(expectedLinkText,link.getText());
    assertEquals("underline",link.getCssValue("text-decoration"));

    link.click();
    verifyPageOpening(expectedUrlParameter,expectedNumberOfWindows);
  }

  public void proceedToForgotPasswordPage(String phoneNumber) {
    proceedToVerificationPage(phoneNumber,acceptablePassword);
    proceedToVerificationPage(phoneNumber,generatePassword());

    wait.until(ExpectedConditions.elementToBeClickable(forgotPasswordLinkLocator));
    driver.findElement(forgotPasswordLinkLocator).click();

    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(lostPasswordPageLocator));
  }

  public void verifyButtonTextAndTextTransform(String expectedButtonText, WebElement button) {
    expectedButtonText = expectedButtonText.toLowerCase();
    String actualButtonText = button.getText().toLowerCase();

    if (actualButtonText.equals("")) {
      actualButtonText = button.getAttribute("value").toLowerCase();
    }

    assertEquals(expectedButtonText,actualButtonText);
    assertEquals("uppercase", button.getCssValue("text-transform"));
  }

  public Boolean returnsTrueIfAccountAlreadyExist(String phoneNumber, String password) {
    if (driver.findElements(errorMessageLocator).size() != 0) {
      WebElement errorMessage = driver.findElement(errorMessageLocator);

      if (errorMessage.getText()
          .equals("Account already exist for this phone number, forgot password?")) {
        return true;
      } else {
        fail("Account registration failed due to error: " + errorMessage.getText()
            + " (phone number: " + phoneNumber + " ,password: " + password + ")");
      }
    }
    return false;
  }

  public String proceedToVerificationPageRetryIfAccountAlreadyExist() {
    String phoneNumber = null;
    int maxTries = 5;

    for (int i = 0; i < maxTries ; i++){
      phoneNumber = generatePhoneNumber("4");

      proceedToVerificationPage(phoneNumber,acceptablePassword);
      checkVerificationPage(phoneNumber);

      if (!returnsTrueIfAccountAlreadyExist(phoneNumber,acceptablePassword)) break;

      if (i == maxTries-1) {
        fail("Failed to sign up "+maxTries+" times in a row with different phone numbers.");
      }
    }
    return phoneNumber;
  }
}
